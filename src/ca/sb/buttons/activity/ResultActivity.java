package ca.sb.buttons.activity;

import java.util.*;

import ca.sb.buttons.R;
import ca.sb.buttons.generators.*;
import ca.sb.buttons.scores.*;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;


public class ResultActivity extends Activity
{
  static Context appContext;
  
  ListView listViewScores;
  List<Score> scoreList;
  ScoreAdapter scoreAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result);

    setTitle("SB Score Results");
    appContext = this;

    scoreList = new Select().all().from(Score.class).execute();
    if(scoreList == null || scoreList.size() == 0)
    	scoreList = new ArrayList<Score>();
    
    listViewScores = (ListView)findViewById(R.id.listViewScores);
    scoreAdapter = new ScoreAdapter(this, scoreList);
    listViewScores.setAdapter(scoreAdapter);

    setupActionBar();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.result_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case android.R.id.home:
      NavUtils.navigateUpFromSameTask(this);
      return true;
      // -------------------------------------------
      // Sort the game scores by best results first
      // -----------------------------------------
    case R.id.best_results_first:
      //list = fileUtils.readAllScores();
    	scoreList = new Select().all().from(Score.class).execute();
      if (scoreList != null)
      {
        Collections.sort(scoreList, Score.BEST_SCORE_FIRST);
        scoreAdapter.clear();
        scoreAdapter.addAll(scoreList);
      }
      Toast.makeText(appContext, "Best Results First", Toast.LENGTH_SHORT)
          .show();
      return true;

    case R.id.worst_results_first:
    	scoreList = new Select().all().from(Score.class).execute();
      if (scoreList != null)
      {
        Collections.sort(scoreList, Score.WORST_SCORE_FIRST);
        scoreAdapter.clear();
        scoreAdapter.addAll(scoreList);
      }
      Toast.makeText(appContext, "Worst Results First", Toast.LENGTH_SHORT)
          .show();
      return true;

    case R.id.latest_results_first:
    	scoreList = new Select().all().from(Score.class).execute();
      if (scoreList != null)
      {
        Collections.sort(scoreList, Score.NEWEST_DATE_FIRST);
        scoreAdapter.clear();
        scoreAdapter.addAll(scoreList);
      }
      Toast.makeText(appContext, "Latest Results First", Toast.LENGTH_SHORT)
          .show();
      return true;

    case R.id.oldest_results_first:
    	scoreList = new Select().all().from(Score.class).execute();
      if (scoreList != null)
      {
        Collections.sort(scoreList, Score.OLDEST_DATE_FIRST);
        scoreAdapter.clear();
        scoreAdapter.addAll(scoreList);
      }
      Toast.makeText(appContext, "Oldest Results First", Toast.LENGTH_SHORT)
          .show();
      return true;

    case R.id.generate_results:
    	scoreList = new ScoreGenerator().getListOfScores(10); //fileUtils.readAllScores();
      if (scoreList != null) //list != null)
      {
    	  for(Score score : scoreList)
    		  score.save();
          scoreAdapter.clear();
          scoreAdapter.addAll(scoreList);
      }
      Toast.makeText(appContext, "Generate Results", Toast.LENGTH_SHORT).show();
      return true;

    case R.id.delete_results:
      scoreList = new Select().all().from(Score.class).execute();
      
      for(Score score : scoreList)
      {
    	  Long id = score.getId();
    	  new Delete().from(Score.class).where("Id = ?", id).executeSingle();
      }
      Toast.makeText(appContext, "Delete Results", Toast.LENGTH_SHORT).show();
      return true;
    }
    return false;
  }
  
  private void setupActionBar()
  {

    getActionBar().setDisplayHomeAsUpEnabled(true);

  }

}
