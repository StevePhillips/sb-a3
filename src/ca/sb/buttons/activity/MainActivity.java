// ------------------------------------------------------
// MainActivity.java
// --------------------------------
// Modified by Steve Bennett to work with local Databases
// ------------------------------------------------------
package ca.sb.buttons.activity;
// ----------------------------------
import java.util.Date;
import java.util.Random;

import ca.sb.buttons.R;
import ca.sb.buttons.scores.*;

// -----------------------
// -----------------------------------
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
// -------------------------------
public class MainActivity extends Activity
{
  public static Context appContext;	
  FileUtils fileUtils = new FileUtils();
  
  Button button1, button2, button3, button4, button5, button6, button7;
  EditText editText1;
  TextView textView3, textView4, textView5;
  Random rand;
  int wins = 0;
  int total = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    appContext = getApplicationContext();
    setTitle("SB Button Game");

    button1 = (Button) findViewById(R.id.button1);
    button2 = (Button) findViewById(R.id.button2);
    button3 = (Button) findViewById(R.id.button3);
    button4 = (Button) findViewById(R.id.button4);
    button5 = (Button) findViewById(R.id.button5);
    button6 = (Button) findViewById(R.id.button6);
    button7 = (Button) findViewById(R.id.button7);

    textView3 = (TextView) findViewById(R.id.textView3);
    textView4 = (TextView) findViewById(R.id.textView4);
    textView5 = (TextView) findViewById(R.id.textView5);

    button1.setOnClickListener(myButtonListener);
    button2.setOnClickListener(myButtonListener);
    button3.setOnClickListener(myButtonListener);
    button4.setOnClickListener(myButtonListener);
    button5.setOnClickListener(myButtonListener);
    button6.setOnClickListener(myButtonListener);
    button7.setOnClickListener(myButtonListener);

    rand = new Random();
  }
  
  private OnClickListener myButtonListener = new OnClickListener()
  {
    public void onClick(View v)
    {
      switch (v.getId())
      {
      case R.id.button1:
        textView3.setText("1");
        calculate();
        break;
      case R.id.button2:
        textView3.setText("2");
        calculate();
        break;
      case R.id.button3:
        textView3.setText("3");
        calculate();
        break;
      case R.id.button4:
        textView3.setText("4");
        calculate();
        break;
      case R.id.button5:
        textView3.setText("5");
        calculate();
        break;
      case R.id.button6:
        textView3.setText("6");
        calculate();
        break;
      case R.id.button7:
        Score score = new Score(wins, total, new Date().getTime());
        score.save();
        textView3.setText("0");
        textView4.setText("0");
        textView5.setText("Bye Bye");
        wins = total = 0;
        break;
      }
    }
  };

  private void calculate()
  {
    String strGuess = textView3.getText().toString();
    try
    {
      int yourGuess = Integer.parseInt(strGuess);
      int iSecret = 1 + rand.nextInt(6);

      if (1 <= yourGuess && yourGuess <= 6)
      {
        textView4.setText("" + iSecret);
        if (iSecret == yourGuess)
        {
          wins++;
          total++;
          textView5.setText("You Won " + wins + "/" + total);
        }
        else
        {
          total++;
          textView5.setText("You Lost " + wins + "/" + total);
        }
      }
      else
      {
        editText1.setText("0");
        textView5.setText("Your Score " + wins + "/" + total);
      }
    }
    catch(Exception e )
    {
      textView3.setText("0");
      textView5.setText("Your Score " + wins + "/" + total);
    }
  }
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main_menu, menu);
    return true;
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.action_view_results:
    	long date = new Date().getTime();
    	new Score(wins, total, date);
      Toast.makeText(appContext, "Go To Child", Toast.LENGTH_SHORT).show();
      startActivity(new Intent(this, ResultActivity.class));
      return true;
    }
    return false;
  }
}
