package ca.sb.buttons.scores;

import java.util.*;
import java.io.*;

import ca.sb.buttons.activity.MainActivity;
import ca.sb.buttons.scores.*;



public class FileUtils
{
  private final String FILENAME = "gamedata.txt";

  /////////////////////////////////////////////////////////////////
  public void storeListOfScores(List<Score> list)
  {

    FileWriter fileWriter = null;
    BufferedWriter bufferedWriter = null;
    PrintWriter outFile = null;
    try
    {
      File path = new File(MainActivity.appContext.getFilesDir(), "/");
      File myFile = new File(path, FILENAME);
      
      fileWriter = new FileWriter(myFile);
      //fileWriter = new FileWriter(FILENAME);
      bufferedWriter = new BufferedWriter(fileWriter);
      outFile = new PrintWriter(bufferedWriter);


      for (int pos = 0; (list != null) && pos < list.size(); pos++)
      {
        Score score = list.get(pos);
        String str = score.getiSuccess() + "|";
        str += score.getiTotal() + "|";
        str += score.getDate();
        if (pos < list.size() - 1)
          str += "\n";
        outFile.write(str);
      }
      bufferedWriter.close();
      fileWriter.close();
    }
    catch(IOException exp )
    {
      System.out.println(exp.getMessage());
      System.out.println(exp.getStackTrace());
    }
    outFile.close();
  }

  /////////////////////////////////////////////////////////////////
  public List<Score> readAllScores()
  {
    List<Score> list = new ArrayList<Score>();

    try
    {
      File path = new File(MainActivity.appContext.getFilesDir(), "/");
      File myFile = new File(path, FILENAME); 
      
      Scanner fileScan = new Scanner(myFile);
      while (fileScan.hasNext())
      {
        String strLine = fileScan.nextLine();

        Scanner stringScan = new Scanner(strLine);
        stringScan.useDelimiter("\\|");

        int success = stringScan.nextInt();
        int total = stringScan.nextInt();
        long date = stringScan.nextLong();

        Score score = new Score(success, total, date);
        list.add(score);
        stringScan.close();
      }
      fileScan.close();
    }

    catch(IOException exp )
    {
      System.out.println(exp.getMessage());
    }

    return list;
  }

  /////////////////////////////////////////////////////////////////
  public void storeAdditionalScore(Score score)
  {
    List<Score> list = readAllScores();
    list.add(score);
    storeListOfScores(list);
  }

  /////////////////////////////////////////////////////////////////
  public String displayListOfScores(List<Score> list)
  {
    String str = "";

    for (int pos = 0; (list != null) && pos < list.size(); pos++)
    {
      str += list.get(pos).toString();
      if (pos < list.size() - 1)
        str += "\n";
    }

    return str;
  }
}
