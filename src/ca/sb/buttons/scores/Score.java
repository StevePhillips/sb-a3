package ca.sb.buttons.scores;

import java.util.*;

import com.activeandroid.Model;
import com.activeandroid.annotation.*;

@Table(name = "Scores")
public class Score extends Model
{
	@Column(name = "Success")
  private int success;
	@Column(name = "Total")
  private int total;
	@Column(name = "Date")
  private long date;
	@Column(name = "Ratio")
  private double ratio;

  public Score(int iSuccess, int iTotal, long date)
  {
	  super();
    this.success = iSuccess;
    this.total = iTotal;
    this.date = date;
    this.ratio = (double)iSuccess / (double)iTotal;
  }
  public Score()
  {
    super();
  }
  public int getiSuccess()
  {
    return success;
  }

  public int getiTotal()
  {
    return total;
  }

  public long getDate()
  {
    return date;
  }

  public double getRatio()
  {
    return ratio;
  }

  public String toString()
  {
    String str = "";
    str += String.format("  %3.3f  ", ratio);
    str += String.format("    %02d   ", success);
    str += String.format("    %03d   ", total);

    Date datum = new Date(date);
    Calendar cal = Calendar.getInstance();
    cal.setTime(datum);
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int month = cal.get(Calendar.MONTH);
    int year = cal.get(Calendar.YEAR);

    String sday = String.format("%02d", day + 1);
    String smonth = String.format("%02d", month + 1);
    String syear = String.format("%04d", year);
    str += sday + "/" + smonth + "/" + syear;
    return str;
  }

  // COMPARATORS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  public static final Comparator<Score> BEST_SCORE_FIRST = new Comparator<Score>()
  {
    public int compare(Score score1, Score score2)
    {
      if (score1.ratio < score2.ratio)
        return 1;
      else if (score1.ratio > score2.ratio)
        return -1;
      else
        return 0;
    }
  };


  public static final Comparator<Score> WORST_SCORE_FIRST = new Comparator<Score>()
  {
    public int compare(Score score1, Score score2)
    {
      if (score1.ratio < score2.ratio)
        return -1;
      else if (score1.ratio > score2.ratio)
        return 1;
      else
        return 0;
    }
  };

  public static final Comparator<Score> NEWEST_DATE_FIRST = new Comparator<Score>()
  {
    public int compare(Score score1, Score score2)
    {
      if (score1.date < score2.date)
        return 1;
      else if (score1.date > score2.date)
        return -1;
      else
        return 0;
    }
  };

  public static final Comparator<Score> OLDEST_DATE_FIRST = new Comparator<Score>()
  {
    public int compare(Score score1, Score score2)
    {
      if (score1.date < score2.date)
        return -1;
      else if (score1.date > score2.date)
        return 1;
      else
        return 0;
    }
  };


}
