package ca.sb.buttons.generators;

import java.util.*;

import ca.sb.buttons.scores.*;


public class ScoreGenerator
{
  Random random;
  private final int MAGIC = 6500000;
  
  public ScoreGenerator()
  {
    random = new Random();
  }
  
  public Score getSingleScore()
  {
    final int MAX = 100;
    int success = random.nextInt(MAX);
    int total =  success + random.nextInt(MAX);
    long date = (long)Math.abs(random.nextLong() / MAGIC);
    
    return new Score(success, total, date);
  }
  
  public List<Score> getListOfScores(int total)
  {
    List<Score> list = new ArrayList<Score>(total);
    
    for (int pos = 0; pos < total; pos++)
    {
      list.add(getSingleScore());
    }
    
    return list;
  }
  

}
